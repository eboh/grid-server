﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GridApp.Infrastructure;
using GridApp.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GridApp.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SiteCorsPolicy")]
    public class UsersController : Controller
    {
        private ApplicationContext _context;

        public UsersController(ApplicationContext context)
        {
            _context = context;
        }

        [Route("")]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.Users.ToList());
        }

        [Route("{id:int}")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            User user = _context.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
                return Ok(user);

            return NotFound();
        }

        [Route("")]
        [HttpPost]
        public IActionResult Post([FromBody]User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return Ok(user);
        }

        [Route("{id:int}")]
        [HttpPut]
        public IActionResult Put(int id, [FromBody]User user)
        {
            User u = _context.Users.Find(id);
            if (u != null)
            {
                u.FirstName = user.FirstName;
                u.LastName= user.LastName;
                u.Email= user.Email;
                _context.Entry(u).State = EntityState.Modified;
                _context.SaveChanges();
                return Ok(u);
            }
            return NotFound();
        }

        [Route("/{id:int}")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            User user = _context.Users.Find(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
                return Ok(user);
            }

            return NotFound();
        }
        
    }
}
